import { ProductResponse } from "@/types/response.types";
import axios from "axios";

export class ProductServices {
  private baseurl = "https://thawing-scrubland-03171.herokuapp.com/https://skincare-api.herokuapp.com/";

  /**
   * @author Michaël Nde
   * @description Searches brand, name, and ingredients for LIKE values
   * @param {string} q Brand or ingredient name to be queried
   * @returns 
   */
  async searchProduct(q: string): Promise<ProductResponse> {
    return axios.get(`${this.baseurl}/product?q=${q}`);
  }
}
