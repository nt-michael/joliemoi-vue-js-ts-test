import { createApp } from "vue";
import App from "./App.vue";
import Toaster from "@meforma/vue-toaster";
import VueAxios from "vue-axios";
import axios from "axios";
createApp(App)
  .use(Toaster, { position: "top", dismissible: true })
  .use(VueAxios, axios)
  .mount("#app");
