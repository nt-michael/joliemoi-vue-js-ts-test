export type ProductResponse = {
    id:number;
    brand:string;
    name:string;
    ingredient_list:string[];
}